package de.s0na.sthopbackend

import org.json.JSONObject
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ProductResourcesTest {

	private val logger = LoggerFactory.getLogger(ProductResourcesTest::class.java)

	@Autowired
	lateinit var testRestTemplate: TestRestTemplate

	@Test
	fun `test product creation`() {
		restTemplate = testRestTemplate

		category(name = "category 44") {
			category(name = "category 45") {
				product(name = "product 1", price = 0.56f, available = true)
			}
		}
	}

	@Test
	fun `test get product`() {
		restTemplate = testRestTemplate

		var productId: Long? = null
		category(name = "category 89") {
			category(name = "category 67")
			category(name = "category 56") {
				productId = product(name = "product 2", price = 0.66f, available = true).id
			}
		}
		requireNotNull(productId)

		val product = testRestTemplate.getForEntity("/api/v1/product/$productId", String::class.java).body
		val parentObject = JSONObject(product)
		logger.info("$parentObject")
		requireNotNull(parentObject)
		requireNotNull(parentObject["id"])
	}

	// TODO write tests for all endpoints

}