package de.s0na.sthopbackend

import org.json.JSONArray
import org.json.JSONObject
import org.junit.Test
import org.junit.runner.RunWith
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class CategoryResourcesTest {

	private val logger = LoggerFactory.getLogger(CategoryResourcesTest::class.java)

	@Autowired
	lateinit var testRestTemplate: TestRestTemplate

	@Test
	fun `test category creation`() {
		restTemplate = testRestTemplate

		category(name = "category 1") {
			category(name = "category 2")
		}
	}

	@Test
	fun `test get category`() {
		restTemplate = testRestTemplate

		val parentId = category(name = "category 12") {
			category(name = "category 23")
			category(name = "category 24")
		}.id

		val parent = testRestTemplate.getForEntity("/api/v1/category/$parentId", String::class.java).body
		val parentObject = JSONObject(parent)
		logger.info("$parentObject")
		requireNotNull(parentObject)
		requireNotNull(parentObject["id"])

		val response = testRestTemplate.getForEntity("/api/v1/category?parentId=$parentId", String::class.java).body
		val responseObject = JSONObject(response)
		requireNotNull(responseObject)

		require((responseObject["content"] as JSONArray).length() == 2)
	}

	// TODO write tests for all endpoints

}