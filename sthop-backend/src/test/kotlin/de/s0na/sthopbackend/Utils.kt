package de.s0na.sthopbackend

import de.s0na.sthopbackend.dto.CategoryDto
import de.s0na.sthopbackend.dto.ProductDto
import org.slf4j.LoggerFactory
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType

private val logger = LoggerFactory.getLogger("Utils")

lateinit var restTemplate: TestRestTemplate

fun category(
	name: String,
	description: String = "$name description",
	parent: Long? = null,
	follow: CategoryDto.() -> Unit = {}
): CategoryDto {
	val requestBody =
		parent?.let {
			"""{
			|  "name": "$name",
			|  "description": "$description",
			|  "parent": $parent
			|}
		""".trimMargin("|")
		} ?: """{
			|  "name": "$name",
			|  "description": "$description"
			|}
		""".trimMargin("|")

	val headers = HttpHeaders().apply {
		contentType = MediaType.APPLICATION_JSON
	}
	val httpEntity = HttpEntity(requestBody, headers)
	val category =
		restTemplate.postForEntity("/api/v1/category", httpEntity, CategoryDto::class.java).body
	requireNotNull(category)
	logger.info("created $category")
	category.follow()
	return category
}

fun CategoryDto.category(
	name: String,
	description: String = "$name description",
	follow: CategoryDto.() -> Unit = {}
): CategoryDto {
	return category(name, description, id, follow)
}

fun CategoryDto.product(
	name: String,
	price: Float,
	available: Boolean,
	description: String = "$name description"
): ProductDto {
	val requestBody = """{
			|  "name": "$name",
			|  "description": "$description",
			|  "parent": $id,
			|  "price": $price,
			|  "available": $available
			|}
		""".trimMargin("|")

	val headers = HttpHeaders().apply {
		contentType = MediaType.APPLICATION_JSON
	}
	val httpEntity = HttpEntity(requestBody, headers)
	val product =
		restTemplate.postForEntity("/api/v1/product", httpEntity, ProductDto::class.java).body
	requireNotNull(product)
	logger.info("created $product")
	return product
}