-- Table category

-- id sequence
CREATE SEQUENCE seq_category_id
  INCREMENT BY 1
  START WITH 1;

-- table
CREATE TABLE category (
  id             BIGINT                   NOT NULL,
  name           VARCHAR(512)             NOT NULL,
  description    VARCHAR(2048)            NOT NULL,
  parent         BIGINT,
	path           VARCHAR(1024)            NOT NULL,
	created        TIMESTAMP WITH TIME ZONE NOT NULL,
  CONSTRAINT pk_category PRIMARY KEY (id),
  CONSTRAINT fk_category_parent
    FOREIGN KEY (parent)
    REFERENCES category (id)
);