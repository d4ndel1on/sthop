-- Table product

-- id sequence
CREATE SEQUENCE seq_product_id
  INCREMENT BY 1
  START WITH 1;

-- table
CREATE TABLE product (
  id             BIGINT                   NOT NULL,
  name           VARCHAR(512)             NOT NULL,
  description    VARCHAR(2048)            NOT NULL,
  category       BIGINT                   NOT NULL,
  price          FLOAT                    NOT NULL,
  available      BOOLEAN                  NOT NULL,
	created        TIMESTAMP WITH TIME ZONE NOT NULL,
  CONSTRAINT pk_product PRIMARY KEY (id),
  CONSTRAINT fk_product_category
    FOREIGN KEY (category)
    REFERENCES category (id)
);