package de.s0na.sthopbackend.repository

import de.s0na.sthopbackend.data.Product
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository

interface ProductRepository : PagingAndSortingRepository<Product, Long> {

	fun findOneById(id: Long): Product?

	fun findByParent_PathLikeOrParent_Id(categoryPath: String, parentId: Long, pageable: Pageable): Page<Product>

	fun findByParent_Id(parentId: Long, pageable: Pageable): Page<Product>

	@Query(
		value = "select p.* from product p join category c on c.id = p.category where lower(p.name) like lower(?1) and (p.category = ?2 or c.path like ?3)",
		countQuery = "select count(*) from product p join category c on c.id = p.category where lower(p.name) like lower(?1) and (p.category = ?2 or c.path like ?3)",
		nativeQuery = true
	)
	fun findByNameAndCategory(query: String, category: Long, categoryPath: String, pageable: Pageable): Page<Product>

	@Query(
		value = "select p.* from product p join category c on c.id = p.category where lower(p.name) like lower(?1)",
		countQuery = "select count(*) from product p join category c on c.id = p.category where lower(p.name) like lower(?1)",
		nativeQuery = true
	)
	fun findByName(query: String, pageable: Pageable): Page<Product>

}