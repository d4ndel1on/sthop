package de.s0na.sthopbackend.repository

import de.s0na.sthopbackend.data.Category
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.PagingAndSortingRepository
import org.springframework.stereotype.Repository

@Repository
interface CategoryRepository : PagingAndSortingRepository<Category, Long> {

	fun findOneById(id: Long): Category?

	fun findByParent_Id(parentId: Long?, pageable: Pageable): Page<Category>

	@Query(
		value = "select c.* from category c where lower(c.name) like lower(?1)",
		countQuery = "select count(*) from category c where lower(c.name) like lower(?1)",
		nativeQuery = true
	)
	fun findByName(query: String, pageable: Pageable): Page<Category>

}