package de.s0na.sthopbackend.services

import de.s0na.sthopbackend.data.Product
import de.s0na.sthopbackend.exception.NotFoundException
import de.s0na.sthopbackend.repository.ProductRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
class ProductService(private val productRepository: ProductRepository) {

	fun getProduct(id: Long): Product {
		return productRepository.findOneById(id)
			?: throw NotFoundException("Product", id)
	}

	fun findProductsByCategory(
		categoryId: Long,
		recursive: Boolean,
		pageable: Pageable = PageRequest.of(0, 10, Sort.by("name"))
	): Page<Product> {
		return if (recursive) {
			productRepository.findByParent_PathLikeOrParent_Id("%/$categoryId/%", categoryId, pageable)
		} else {
			productRepository.findByParent_Id(categoryId, pageable)
		}
	}

	fun searchProductsByNameAndParent(
		query: String,
		categoryId: Long? = null,
		pageable: Pageable = PageRequest.of(0, 10, Sort.by("name"))
	): Page<Product> {
		return categoryId?.let {
			productRepository.findByNameAndCategory("%$query%", it, "%/$it/%", pageable)
		} ?: productRepository.findByName("%$query%", pageable)
	}

	fun createProduct(product: Product): Product {
		return productRepository.save(product)
	}

	fun updateProduct(product: Product) {
		productRepository.save(product)
	}

	fun deleteProduct(product: Product) {
		productRepository.delete(product)
	}

}