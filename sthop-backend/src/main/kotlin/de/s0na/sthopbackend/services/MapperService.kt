package de.s0na.sthopbackend.services

import de.s0na.sthopbackend.data.Category
import de.s0na.sthopbackend.data.Product
import de.s0na.sthopbackend.dto.CategoryDto
import de.s0na.sthopbackend.dto.ProductDto
import de.s0na.sthopbackend.exception.NotFoundException
import org.springframework.stereotype.Service

@Service
class MapperService(private val categoryService: CategoryService) {

	fun toEntity(dto: CategoryDto): Category {
		val parent = dto.parent?.let {
			categoryService.findCategory(it)
				?: throw NotFoundException("Category", it)
		}


		val parentPath = parent?.path ?: "/"
		val path = parent?.id?.let { "$parentPath$it/" }
			?: "/"

		return Category(
			id = dto.id,
			name = dto.name,
			description = dto.description,
			parent = parent,
			path = path
		)
	}

	fun toDto(entity: Category): CategoryDto {
		return CategoryDto(
			id = entity.id,
			name = entity.name,
			description = entity.description,
			parent = entity.parent?.id,
			path = entity.path
		)
	}

	fun toEntity(dto: ProductDto): Product {
		val parent = dto.parent.let {
			categoryService.findCategory(it)
				?: throw NotFoundException("Parent Category", it)
		}

		return Product(
			id = dto.id,
			name = dto.name,
			description = dto.description,
			parent = parent,
			available = dto.available,
			price = dto.price
		)
	}

	fun toDto(entity: Product): ProductDto {
		return ProductDto(
			id = entity.id,
			name = entity.name,
			description = entity.description,
			parent = entity.parent.id!!,
			price = entity.price,
			available = entity.available
		)
	}

}