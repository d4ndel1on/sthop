package de.s0na.sthopbackend.services

import de.s0na.sthopbackend.data.Category
import de.s0na.sthopbackend.repository.CategoryRepository
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.data.domain.Sort
import org.springframework.stereotype.Service

@Service
class CategoryService(private val categoryRepository: CategoryRepository) {

	fun findCategory(id: Long): Category? {
		return categoryRepository.findOneById(id)
	}

	fun findByParent(
		parentId: Long?,
		pageable: Pageable = PageRequest.of(0, 10, Sort.by("name"))
	): Page<Category> {
		return categoryRepository.findByParent_Id(parentId, pageable)
	}

	fun searchByName(
		query: String,
		pageable: Pageable = PageRequest.of(0, 10, Sort.by("name"))
	): Page<Category> {
		return categoryRepository.findByName("%$query%", pageable)
	}

	fun createCategory(category: Category): Category {
		return categoryRepository.save(category)
	}

	fun updateCategory(category: Category) {
		categoryRepository.save(category)
	}

	fun deleteCategory(category: Category) {
		categoryRepository.delete(category)
	}

}