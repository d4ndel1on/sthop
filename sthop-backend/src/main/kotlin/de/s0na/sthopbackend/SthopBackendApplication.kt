package de.s0na.sthopbackend

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.databind.SerializationFeature
import com.fasterxml.jackson.datatype.jdk8.Jdk8Module
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule
import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean

@SpringBootApplication
class SthopBackendApplication {

	@Bean
	fun objectMapper(): ObjectMapper {
		return ObjectMapper().apply {
			registerModule(Jdk8Module())
			registerModule(JavaTimeModule())
			registerKotlinModule()
			configure(SerializationFeature.WRITE_DATES_AS_TIMESTAMPS, false)
			configure(DeserializationFeature.FAIL_ON_NULL_FOR_PRIMITIVES, true)
		}
	}

}

fun main(args: Array<String>) {
	runApplication<SthopBackendApplication>(*args)
}
