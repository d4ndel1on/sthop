package de.s0na.sthopbackend.api

import de.s0na.sthopbackend.dto.ErrorDto
import de.s0na.sthopbackend.dto.PageDto
import de.s0na.sthopbackend.dto.ProductDto
import de.s0na.sthopbackend.dto.toDto
import de.s0na.sthopbackend.exception.ValidationException
import de.s0na.sthopbackend.services.MapperService
import de.s0na.sthopbackend.services.ProductService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/api/v1/product")
@Api(value = "/api/v1/product", description = "Product Endpoints", produces = MediaType.APPLICATION_JSON_VALUE)
class ProductResources(private val mapperService: MapperService, private val productService: ProductService) {

	@ApiOperation(value = "get product", response = ProductDto::class)
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Product Details", response = ProductDto::class),
			ApiResponse(code = 500, message = "Internal Server Error"),
			ApiResponse(code = 404, message = "Product not found", response = ErrorDto::class)
		]
	)
	@GetMapping("/{id}", produces = [MediaType.APPLICATION_JSON_VALUE])
	fun getCategory(@PathVariable id: Long): ProductDto {
		val product = productService.getProduct(id)
		return mapperService.toDto(product)
	}

	@ApiOperation(value = "list products by category", response = PageDto::class)
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "List of Product Details", response = PageDto::class),
			ApiResponse(code = 500, message = "Internal Server Error")
		]
	)
	@GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
	fun listByCategory(
		@RequestParam categoryId: Long,
		@RequestParam(defaultValue = "true") recursive: Boolean,
		@RequestParam(defaultValue = "0") page: Int,
		@RequestParam(defaultValue = "10") size: Int,
		@RequestParam(defaultValue = "name") sort: String,
		@RequestParam(defaultValue = "ASC") direction: Sort.Direction
	): PageDto<ProductDto> {
		val pageRequest = PageRequest.of(page, size, Sort.by(direction, sort))
		return productService.findProductsByCategory(categoryId, recursive, pageRequest)
			.map { mapperService.toDto(it) }
			.toDto()
	}

	@ApiOperation(value = "search products by name and category", response = PageDto::class)
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Search Products", response = PageDto::class),
			ApiResponse(code = 500, message = "Internal Server Error")
		]
	)
	@GetMapping("/search", produces = [MediaType.APPLICATION_JSON_VALUE])
	fun searchByName(
		@RequestParam query: String,
		@RequestParam(required = false) categoryId: Long?,
		@RequestParam(defaultValue = "0") page: Int,
		@RequestParam(defaultValue = "10") size: Int,
		@RequestParam(defaultValue = "name") sort: String,
		@RequestParam(defaultValue = "ASC") direction: Sort.Direction
	): PageDto<ProductDto> {
		val pageRequest = PageRequest.of(page, size, Sort.by(direction, sort))
		return productService.searchProductsByNameAndParent(query, categoryId, pageRequest)
			.map { mapperService.toDto(it) }
			.toDto()
	}

	@ApiOperation(value = "create product", response = ProductDto::class)
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Created Product Details", response = ProductDto::class),
			ApiResponse(code = 400, message = "Validation Error", response = ErrorDto::class),
			ApiResponse(code = 404, message = "Category does not exist.", response = ErrorDto::class),
			ApiResponse(code = 500, message = "Internal Server Error")
		]
	)
	@PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
	fun createProduct(@RequestBody @Valid productDto: ProductDto): ProductDto {
		if (productDto.price <= 0) {
			throw ValidationException("Price needs to be positive.")
		}
		val entity = mapperService.toEntity(productDto)
		val product = productService.createProduct(entity)
		return mapperService.toDto(product)
	}


	@ApiOperation(value = "update product")
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Product Updated"),
			ApiResponse(code = 400, message = "Validation Error", response = ErrorDto::class),
			ApiResponse(code = 500, message = "Internal Server Error"),
			ApiResponse(code = 404, message = "Product not found", response = ErrorDto::class)
		]
	)
	@PutMapping("/{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
	fun updateProduct(@PathVariable id: Long, @Valid @RequestBody productDto: ProductDto) {
		if (id != productDto.id) {
			throw ValidationException("Ids did not match.")
		}
		if (productDto.price <= 0) {
			throw ValidationException("Price needs to be positive.")
		}
		val existing = productService.getProduct(id)
		val new = mapperService.toEntity(productDto)
		existing.apply {
			name = new.name
			description = new.description
			parent = new.parent
			price = new.price
			available = new.available
		}
		productService.updateProduct(existing)
	}

	@ApiOperation(value = "delete product")
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Product deleted"),
			ApiResponse(code = 500, message = "Internal Server Error"),
			ApiResponse(code = 404, message = "Product not found")
		]
	)
	@DeleteMapping("/{id}")
	fun deleteProduct(@PathVariable id: Long) {
		val product = productService.getProduct(id)
		productService.deleteProduct(product)
	}

}