package de.s0na.sthopbackend.api

import de.s0na.sthopbackend.dto.ErrorDto
import de.s0na.sthopbackend.exception.NotFoundException
import de.s0na.sthopbackend.exception.ValidationException
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

@ControllerAdvice
class ExceptionHandler {

	@ExceptionHandler(NotFoundException::class)
	fun handleNotFoundException(exception: NotFoundException): ResponseEntity<ErrorDto> {
		return ResponseEntity(ErrorDto(message = exception.message), HttpStatus.NOT_FOUND)
	}

	@ExceptionHandler(ValidationException::class)
	fun handleValidationException(exception: ValidationException): ResponseEntity<ErrorDto> {
		return ResponseEntity.badRequest().body(ErrorDto(exception.message))
	}

}