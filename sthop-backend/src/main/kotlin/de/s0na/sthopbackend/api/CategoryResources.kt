package de.s0na.sthopbackend.api

import de.s0na.sthopbackend.dto.CategoryDto
import de.s0na.sthopbackend.dto.ErrorDto
import de.s0na.sthopbackend.dto.PageDto
import de.s0na.sthopbackend.dto.toDto
import de.s0na.sthopbackend.exception.NotFoundException
import de.s0na.sthopbackend.exception.ValidationException
import de.s0na.sthopbackend.services.CategoryService
import de.s0na.sthopbackend.services.MapperService
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import io.swagger.annotations.ApiResponse
import io.swagger.annotations.ApiResponses
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Sort
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.*
import javax.validation.Valid

@CrossOrigin
@RestController
@RequestMapping("/api/v1/category")
@Api(value = "/api/v1/category", description = "Category Endpoints", produces = MediaType.APPLICATION_JSON_VALUE)
class CategoryResources(private val mapperService: MapperService, private val categoryService: CategoryService) {

	@ApiOperation(value = "get category", response = CategoryDto::class)
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Category Details", response = CategoryDto::class),
			ApiResponse(code = 500, message = "Internal Server Error"),
			ApiResponse(code = 404, message = "Category not found", response = ErrorDto::class)
		]
	)
	@GetMapping("/{id}", produces = [MediaType.APPLICATION_JSON_VALUE])
	fun getCategory(@PathVariable id: Long): CategoryDto {
		val category = categoryService.findCategory(id)
			?: throw NotFoundException("Category", id)
		return mapperService.toDto(category)
	}

	@ApiOperation(value = "list categories by parent", response = PageDto::class)
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "List of Category Details", response = PageDto::class),
			ApiResponse(code = 500, message = "Internal Server Error")
		]
	)
	@GetMapping(produces = [MediaType.APPLICATION_JSON_VALUE])
	fun listByParent(
		@RequestParam(required = false) parentId: Long? = null,
		@RequestParam(defaultValue = "0") page: Int,
		@RequestParam(defaultValue = "10") size: Int,
		@RequestParam(defaultValue = "name") sort: String,
		@RequestParam(defaultValue = "ASC") direction: Sort.Direction
	): PageDto<CategoryDto> {
		val pageRequest = PageRequest.of(page, size, Sort.by(direction, sort))
		return categoryService.findByParent(parentId, pageRequest)
			.map { mapperService.toDto(it) }
			.toDto()
	}

	@ApiOperation(value = "search categories by name", response = PageDto::class)
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Search Categories by Name", response = PageDto::class),
			ApiResponse(code = 500, message = "Internal Server Error")
		]
	)
	@GetMapping("/search", produces = [MediaType.APPLICATION_JSON_VALUE])
	fun searchByName(
		@RequestParam query: String,
		@RequestParam(defaultValue = "0") page: Int,
		@RequestParam(defaultValue = "10") size: Int,
		@RequestParam(defaultValue = "name") sort: String,
		@RequestParam(defaultValue = "ASC") direction: Sort.Direction
	): PageDto<CategoryDto> {
		val pageRequest = PageRequest.of(page, size, Sort.by(direction, sort))
		return categoryService.searchByName(query, pageRequest)
			.map { mapperService.toDto(it) }
			.toDto()
	}

	@ApiOperation(value = "create category", response = CategoryDto::class)
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Created Category Details", response = CategoryDto::class),
			ApiResponse(code = 400, message = "Validation Error", response = ErrorDto::class),
			ApiResponse(code = 404, message = "Parent Category does not exist.", response = ErrorDto::class),
			ApiResponse(code = 500, message = "Internal Server Error")
		]
	)
	@PostMapping(consumes = [MediaType.APPLICATION_JSON_VALUE], produces = [MediaType.APPLICATION_JSON_VALUE])
	fun createCategory(@RequestBody @Valid categoryDto: CategoryDto): CategoryDto {
		val entity = mapperService.toEntity(categoryDto)
		val category = categoryService.createCategory(entity)
		return mapperService.toDto(category)
	}

	@ApiOperation(value = "update category")
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Category Updated"),
			ApiResponse(code = 400, message = "Validation Error", response = ErrorDto::class),
			ApiResponse(code = 500, message = "Internal Server Error"),
			ApiResponse(code = 404, message = "Category not found", response = ErrorDto::class)
		]
	)
	@PutMapping("/{id}", consumes = [MediaType.APPLICATION_JSON_VALUE])
	fun updateCategory(@PathVariable id: Long, @Valid @RequestBody categoryDto: CategoryDto) {
		if (id != categoryDto.id) {
			throw ValidationException("Ids did not match.")
		}
		val existing = categoryService.findCategory(id)
			?: throw NotFoundException("Category", id)
		val new = mapperService.toEntity(categoryDto)
		existing.apply {
			name = new.name
			description = new.description
			parent = new.parent
			path = new.path
		}
		categoryService.updateCategory(existing)
	}

	@ApiOperation(value = "delete category")
	@ApiResponses(
		value = [
			ApiResponse(code = 200, message = "Category deleted"),
			ApiResponse(code = 500, message = "Internal Server Error"),
			ApiResponse(code = 404, message = "Category not found")
		]
	)
	@DeleteMapping("/{id}")
	fun deleteCategory(@PathVariable id: Long) {
		val category = categoryService.findCategory(id)
			?: throw NotFoundException("Category", id)
		categoryService.deleteCategory(category)
	}

}