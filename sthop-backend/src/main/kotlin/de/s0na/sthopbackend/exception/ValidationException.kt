package de.s0na.sthopbackend.exception

class ValidationException(val reason: String): Exception(reason)