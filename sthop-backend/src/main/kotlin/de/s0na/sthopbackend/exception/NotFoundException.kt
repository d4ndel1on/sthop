package de.s0na.sthopbackend.exception

class NotFoundException(type: String, id: Long) : Exception("$type with id $id was not found")