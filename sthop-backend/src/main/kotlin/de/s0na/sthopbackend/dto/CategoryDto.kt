package de.s0na.sthopbackend.dto

import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank

@Validated
data class CategoryDto(

	val id: Long?,

	@NotBlank
	val name: String,

	@NotBlank
	val description: String,

	val parent: Long?,

	val path: String?

)