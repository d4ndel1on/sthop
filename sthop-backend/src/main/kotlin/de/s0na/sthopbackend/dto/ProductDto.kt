package de.s0na.sthopbackend.dto

import org.jetbrains.annotations.NotNull
import org.springframework.validation.annotation.Validated
import javax.validation.constraints.NotBlank

@Validated
data class ProductDto(

	val id: Long?,

	@NotBlank
	val name: String,

	@NotBlank
	val description: String,

	@NotNull
	val parent: Long,

	@NotNull
	val price: Float,

	@NotNull
	val available: Boolean

)