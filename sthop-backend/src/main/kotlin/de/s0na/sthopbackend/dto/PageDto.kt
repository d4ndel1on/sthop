package de.s0na.sthopbackend.dto

import org.springframework.data.domain.Page

fun <T> Page<T>.toDto(): PageDto<T> {
	return PageDto(
		content = content,
		totalPages = totalPages,
		totalElements = totalElements,
		size = size,
		page = number
	)
}

data class PageDto<T>(
	val content: List<T>,
	val page: Int,
	val size: Int,
	val totalElements: Long,
	val totalPages: Int
)