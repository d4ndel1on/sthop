package de.s0na.sthopbackend.dto

import java.time.Instant

data class ErrorDto(val message: String?, val timestamp: Instant = Instant.now())