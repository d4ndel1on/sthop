package de.s0na.sthopbackend.config

import com.google.common.base.Predicate
import com.google.common.base.Predicates
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2


@ConditionalOnProperty("swagger.enabled")
@Configuration
@EnableSwagger2
class SwaggerConfig(@Value("\${app.version}") private val appVersion: String) {

	@Bean
	fun produceApi(): Docket {
		return Docket(DocumentationType.SWAGGER_2)
			.apiInfo(apiInfo())
			.select()
			.apis(RequestHandlerSelectors.basePackage("de.s0na.sthopbackend.api"))
			.paths(paths())
			.build()
	}

	fun apiInfo(): ApiInfo {
		return ApiInfoBuilder()
			.title("Sthop Rest APIs")
			.description("This page lists all rest apis for the Sthop Backend API.")
			.version(appVersion)
			.build()
	}

	private fun paths(): Predicate<String> {
		return Predicates.and(
			PathSelectors.regex("/api/v1/.*")
		)
	}
}