package de.s0na.sthopbackend.data

import java.time.Instant
import javax.persistence.*

const val PRODUCT_ID_SEQUENCE = "seq_product_id"

@Entity
@Table(name = "product")
data class Product(

	@Id
	@SequenceGenerator(name = PRODUCT_ID_SEQUENCE, sequenceName = PRODUCT_ID_SEQUENCE, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = PRODUCT_ID_SEQUENCE)
	val id: Long? = null,

	@Column(name = "name", nullable = false, length = 512)
	var name: String,

	@Column(name = "description", nullable = false, length = 2048)
	var description: String,

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "category")
	var parent: Category,

	@Column(name = "price", nullable = false)
	var price: Float,

	@Column(name = "available", nullable = false)
	var available: Boolean = true,

	@Column(name = "created", nullable = false)
	val created: Instant = Instant.now()

)