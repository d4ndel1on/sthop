package de.s0na.sthopbackend.data

import java.time.Instant
import javax.persistence.*

const val CATEGORY_ID_SEQUENCE = "seq_category_id"

@Entity
@Table(name = "category")
data class Category(

	@Id
	@SequenceGenerator(name = CATEGORY_ID_SEQUENCE, sequenceName = CATEGORY_ID_SEQUENCE, allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = CATEGORY_ID_SEQUENCE)
	val id: Long? = null,

	@Column(name = "name", nullable = false, length = 512)
	var name: String,

	@Column(name = "description", nullable = false, length = 2048)
	var description: String,

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "parent")
	var parent: Category?,

	@Column(name = "path", length = 1024)
	var path: String,

	@Column(name = "created", nullable = false)
	val created: Instant = Instant.now()

)