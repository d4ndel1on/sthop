#!/bin/bash

echo "# remove old frontend"
rm -rf sthop-backend/src/main/resources/static

echo "# change baseurl to empty string"
sed -i "s/export const BASE_URL = '.*';/export const BASE_URL = '';/g" sthop-frontend/src/common/Constants.tsx

echo "# build frontend"
cd sthop-frontend
npm run build
cd ..

echo "# copy content"
mkdir -p sthop-backend/src/main/resources/static
cp -r sthop-frontend/build/* sthop-backend/src/main/resources/static

echo "# build & deploy container"
mvn clean install -Dspring.profiles.active=local

echo "# change baseurl back to local development url"
sed -i "s/export const BASE_URL = '.*';/export const BASE_URL = 'http:\/\/localhost:8080';/g" sthop-frontend/src/common/Constants.tsx