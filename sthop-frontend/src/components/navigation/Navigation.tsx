import React, {Component} from 'react';

import {SimpleTopAppBar} from 'rmwc/TopAppBar';
import '@material/top-app-bar/dist/mdc.top-app-bar.css';
import {Drawer, DrawerContent} from 'rmwc/Drawer';
import {ListItem, ListItemText} from 'rmwc/List';
import './Navigation.css';
import CategoryPage from '../category/CategoryPage';

interface State {
    drawerOpen: boolean
    page: Page
}

enum Page {
    HOME,
    BROWSE
}

class Navigation extends Component<any, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            drawerOpen: false,
            page: Page.HOME
        }
    }

    render() {
        const page = this.state.page;

        let content = (
            <div>
                <h1>Welcome</h1>
                <p>This is Stefan Hoferer's Online Shop</p>
            </div>
        );
        if (page === Page.BROWSE) {
            content = <CategoryPage/>
        }

        return (
            <div>
                <SimpleTopAppBar
                    id="nav"
                    title="sthop"
                    navigationIcon={{onClick: () => this.onToggleDrawer()}}
                />
                <Drawer id="drawer"
                        open={this.state.drawerOpen}
                        onClose={this.onCloseDrawer}
                        modal
                >
                    <DrawerContent id="drawer-content">
                        <ListItem onClick={() => this.redirectTo(Page.HOME)}>
                            <ListItemText>Home</ListItemText>
                        </ListItem>
                        <ListItem onClick={() => this.redirectTo(Page.BROWSE)}>
                            <ListItemText>Browse</ListItemText>
                        </ListItem>
                    </DrawerContent>
                </Drawer>
                <div id="content">
                    {content}
                </div>
            </div>
        )
    }

    private onToggleDrawer = () => {
        this.setState({drawerOpen: !this.state.drawerOpen})
    };

    private onCloseDrawer = () => {
        this.setState({drawerOpen: false})
    };

    private redirectTo(page: Page) {
        this.setState({
            drawerOpen: false,
            page
        })
    }

}

export default Navigation;