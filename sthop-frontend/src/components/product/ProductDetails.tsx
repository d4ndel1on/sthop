import React, {Component} from 'react';

import {Elevation} from '@rmwc/elevation';
import {Button, ButtonIcon} from '@rmwc/button'
import {Grid, GridCell} from '@rmwc/grid';
import {BASE_URL} from '../../common/Constants';

interface Props {
    productId: number
    onBack: () => void
}

interface State {
    loading: boolean
    name?: string
    description?: string
    price?: number
}

class ProductDetails extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            loading: false
        }
    }

    componentWillMount() {
        this.loadData()
    }

    render() {
        console.log('render details');
        let content = <h2>loading...</h2>;

        const {name, description, price} = this.state;

        const encodedName = encodeURIComponent(name!);
        const url = `https://via.placeholder.com/180x180.png?text=${encodedName}`;

        if (!this.state.loading && this.state.name) {
            content = (
                <Grid>
                    <GridCell span={4}>
                        <img src={url}/>
                    </GridCell>
                    <GridCell span={3}>
                        <h3>{name}</h3>
                    </GridCell>
                    <GridCell span={3}>
                        <h4>{description}</h4>
                    </GridCell>
                    <GridCell span={2}>
                        <h4>{price} €</h4>
                    </GridCell>
                </Grid>
            );
        }

        return (
            <div>
                <Elevation z={12} className="page-title">Product</Elevation>
                <Elevation z={10} className="page-content">
                    {content}
                </Elevation>
                <Button className="page-footer" raised onClick={this.props.onBack}>
                    <ButtonIcon icon="arrow_back"/>
                    Back
                </Button>
            </div>
        );

    }

    private loadData() {
        this.setState({loading: true});
        fetch(`${BASE_URL}/api/v1/product/${this.props.productId}`)
            .then(response => response.json())
            .then((response: any) => this.setState({
                name: response.name,
                description: response.description,
                price: response.price,
                loading: false
            }));
    }

}

export default ProductDetails;