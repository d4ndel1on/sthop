import React, {Component} from 'react';

import {Elevation} from '@rmwc/elevation';
import {Button, ButtonIcon} from '@rmwc/button'
import {TextField} from '@rmwc/textfield';
import {BASE_URL} from '../../common/Constants';
import {Select} from '@rmwc/select';
import {Product} from '../../common/Entities';
import '@material/select/dist/mdc.select.css';
import '@material/floating-label/dist/mdc.floating-label.css';
import '@material/notched-outline/dist/mdc.notched-outline.css';
import '@material/line-ripple/dist/mdc.line-ripple.css';
import {priceToEuro} from '../../common/CurrencyService';

interface State {
    name: string
    description: string
    price: number
    currency: string
}

interface Props {
    editId?: number
    parent: number
    onBack: () => void
}

class ProductForm extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            name: '',
            description: '',
            price: 0,
            currency: 'EUR'
        }
    }

    render() {
        const {name, description, price, currency} = this.state;
        const title = this.props.editId ? 'Edit Product' : 'Create Product';

        return (
            <div>
                <Elevation z={12} className="page-title">{title}</Elevation>
                <Elevation z={10} className="page-content">
                    <TextField
                        id="product_name"
                        value={name}
                        onChange={(event: any) => this.setState({name: event.target.value})}
                        required
                        label="Name"/>
                    <br/>
                    <TextField
                        id="product_description"
                        value={description}
                        required
                        onChange={(event: any) => this.setState({description: event.target.value})}
                        label="Description"/>
                    <Select
                        style={{margin: "24px"}}
                        label="Standard"
                        placeholder=""
                        value={currency}
                        onChange={(event: any) => this.setState({currency: event.target.value})}
                        options={['EUR', 'BTC', 'USD', 'CHF', 'CAD']}
                    />
                    <TextField
                        id="product_price"
                        value={price}
                        required
                        type="number"
                        min={0.000000001}
                        step={0.0000000001}
                        onChange={(event: any) => this.setState({price: event.target.value})}
                        label="Price"/>
                </Elevation>
                <Button className="page-footer" raised onClick={this.onSave}>
                    <ButtonIcon icon="save"/>
                    Save
                </Button>
                <Button className="page-footer" raised onClick={this.props.onBack}>
                    <ButtonIcon icon="arrow_back"/>
                    Back
                </Button>
            </div>
        )

    }

    private onSave = () => {
        priceToEuro(this.state.price, this.state.currency).then(price => {
            const {editId} = this.props;

            let method = 'POST';
            if (editId) {
                method = 'PUT';
            }

            let body: Product = {
                id: editId,
                name: this.state.name,
                description: this.state.description,
                price: price,
                available: true,
                parent: this.props.parent
            };
            body.name = this.state.name;
            body.description = this.state.description;
            body.parent = this.props.parent;

            fetch(`${BASE_URL}/api/v1/product`,
                {
                    method: method,
                    headers: {
                        'Accept': 'application/json',
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(body)
                })
                .then(response => response.json())
                .then(() => this.props.onBack())
        });
    };

}

export default ProductForm;
