import React, {Component} from 'react';

import {Button, ButtonIcon} from '@rmwc/button'
import {Category, Page, Product} from '../../common/Entities';
import {Elevation} from '@rmwc/elevation';
import {Snackbar} from '@rmwc/snackbar';
import {
    DataTable,
    DataTableBody,
    DataTableCell,
    DataTableContent,
    DataTableHead,
    DataTableHeadCell,
    DataTableRow
} from '@rmwc/data-table';
import {BASE_URL} from '../../common/Constants';
import '@material/snackbar/dist/mdc.snackbar.css';
import '@material/dialog/dist/mdc.dialog.css';
import '@material/button/dist/mdc.button.css';
import {SimpleDialog} from '@rmwc/dialog';

interface State {
    parent?: number
    parents: number[]
    loading: boolean
    content: any[]
    data?: any
    error?: any
    confirmDelete: boolean
    deleteId?: number
    deleteType?: string
}

interface Props {
    onEdit: (id?: number, parent?: number) => void
    onBack: () => void
    onCreateProduct: (parent: number, id?: number) => void
    onShowProduct: (id: number) => void
}


class CategoryList extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            loading: false,
            content: [],
            parents: [],
            confirmDelete: false
        }
    }

    componentWillMount() {
        this.loadContent();
    }

    componentDidUpdate(_: Props, prevState: State) {
        if (prevState.parent != this.state.parent) {
            this.loadContent();
        }
    }

    render() {
        if (this.state.loading) {
            return <h2>Loading...</h2>;
        }

        let content = (
            <DataTable>
                <DataTableContent>
                    <DataTableHead>
                        <DataTableRow>
                            <DataTableHeadCell/>
                            <DataTableHeadCell>Name</DataTableHeadCell>
                            <DataTableHeadCell>Description</DataTableHeadCell>
                            <DataTableHeadCell>Price</DataTableHeadCell>
                            <DataTableHeadCell/>
                        </DataTableRow>
                    </DataTableHead>
                    <DataTableBody>
                        {this.state.content.map((item, i) => (
                            <DataTableRow key={i}>
                                <DataTableCell>
                                    <ButtonIcon onClick={this.onItemSelected(item)}
                                                icon="chevron_right"/>
                                </DataTableCell>
                                <DataTableCell style={{width: "270px"}}>{item.name}</DataTableCell>
                                <DataTableCell style={{width: "320px"}}>{item.description}</DataTableCell>
                                <DataTableCell
                                    style={{width: "120px"}}>{item.price ? (item.price + '€') : ''}</DataTableCell>
                                <DataTableCell>
                                    <Button raised onClick={() => this.onDeleteItem(item.id, item.type)}>
                                        <ButtonIcon icon="delete_outlined"/>
                                        Delete
                                    </Button>
                                    {
                                        item.type === 'category' ?
                                            <Button onClick={this.onCreateCategory(item.id)}>
                                                <ButtonIcon icon="add"/>
                                                Category
                                            </Button>
                                            : null
                                    }
                                    {
                                        item.type === 'category' ?
                                            <Button onClick={this.onCreateProduct(item.id)}>
                                                <ButtonIcon icon="add"/>
                                                Product
                                            </Button>
                                            : null
                                    }
                                </DataTableCell>
                            </DataTableRow>
                        ))}
                    </DataTableBody>
                </DataTableContent>
            </DataTable>
        );

        if (this.state.content.length == 0) {
            content = <h2>Nothing found.</h2>;
        }

        return (
            <div>
                <SimpleDialog
                    title="Confirmation"
                    body="Only Items without cildren can be deleted. This deletion is permanent."
                    open={this.state.confirmDelete}
                    onClose={(evt: any) => {
                        this.setState({confirmDelete: false});
                        if (evt.detail.action === 'accept') {
                            this.onDeletionConfirmed()
                        } else if (evt.detail.action === 'close') {
                            this.setState({
                                confirmDelete: false,
                                deleteId: undefined,
                                deleteType: undefined
                            })
                        }
                    }}
                />
                <Snackbar
                    show={this.state.error}
                    onHide={() => this.setState({error: undefined})}
                    message={this.state.error}
                    timeout={4000}
                />
                <Elevation z={12} className="page-title">Categories</Elevation>
                <Elevation z={10} className="page-content">
                    {content}
                </Elevation>

                {
                    !this.state.parent ?
                        <Button className="page-footer" raised onClick={this.onCreateCategory()}><ButtonIcon
                            icon="add"/> Create</Button>
                        : null
                }
                {
                    this.state.parent && this.state.parents.length > 0 ?
                        <Button className="page-footer" raised onClick={this.onUp}>
                            <ButtonIcon icon="arrow_upward"/>
                            Up
                        </Button>
                        : null
                }
                {
                    this.state.parent ?
                        <Button className="page-footer" raised onClick={this.onBack}>
                            <ButtonIcon icon="arrow_back"/>
                            Back
                        </Button>
                        : null
                }
            </div>
        );
    }

    private onCreateCategory(parent?: number) {
        return () => {
            this.props.onEdit(undefined, parent)
        };
    }

    private onDeletionConfirmed() {
        const id = this.state.deleteId;
        const type = this.state.deleteType;
        const url = `${BASE_URL}/api/v1/${type}/${id}`;

        fetch(url, {method: 'DELETE'})
            .then((response) => {
                if (response.status >= 300) {
                    this.onError('Deletion failed.')
                } else {
                    this.onBack();
                }
            })
    }

    private onError(error: string) {
        this.setState({
            confirmDelete: false,
            deleteType: undefined,
            deleteId: undefined,
            error
        });
    }

    private onCreateProduct(parent: number) {
        return () => {
            this.props.onCreateProduct(parent)
        }
    }

    private onDeleteItem(id: number, type: string) {
        this.setState({
            confirmDelete: true,
            deleteId: id,
            deleteType: type
        });
    }

    private onItemSelected(item: any) {
        if (item.type === 'category') {
            return () => {
                this.setState({
                    parent: item.id,
                    parents: this.state.parents.concat(item.id!)
                });
            }
        }
        return () => {
            this.props.onShowProduct(item.id)
        }
    }

    private onUp = () => {
        let newParents = [...this.state.parents];
        newParents.pop();
        this.setState({
            content: [],
            parent: newParents[newParents.length - 1],
            parents: newParents
        });
    };

    private onBack = () => {
        this.setState({
            content: [],
            parent: undefined,
            confirmDelete: false,
            parents: []
        });
    };

    private loadContent() {
        if (this.state.loading) {
            return
        }
        let categoryUrl = `${BASE_URL}/api/v1/category?size=5000`;
        let productUrl = undefined;
        if (this.state.parent) {
            categoryUrl = `${BASE_URL}/api/v1/category?parentId=${this.state.parent}&size=5000`;
            productUrl = `${BASE_URL}/api/v1/product?categoryId=${this.state.parent}&size=5000&recursive=false`;
        }
        this.setState({loading: true});

        let promises = [
            fetch(categoryUrl)
                .then(response => response.json())
                .then((response: Page<Category>) => response.content)
        ];

        if (productUrl) {
            promises.push(fetch(productUrl)
                .then(response => response.json())
                .then((response: Page<Product>) => response.content))
        }

        Promise.all(promises).then((responses: any[]) => {
            let values: any[] = [];

            responses[0].forEach((value: any) => {
                value.type = 'category';
                values.push(value);
            });

            if (responses.length > 0 && responses[1]) {
                responses[1].forEach((value: any) => {
                    value.type = 'product';
                    values.push(value)
                });
            }

            this.setState({
                loading: false,
                content: values
            });
        });

    }
}

export default CategoryList;