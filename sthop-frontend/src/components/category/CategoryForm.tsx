import React, {Component} from 'react';

import {Elevation} from '@rmwc/elevation';
import {Button, ButtonIcon} from '@rmwc/button'
import {TextField} from '@rmwc/textfield';
import {BASE_URL} from '../../common/Constants';

interface State {
    name: string
    description: string
}

interface Props {
    editId?: number
    parent?: number
    onBack: () => void
}

class CategoryForm extends Component<Props, State> {

    constructor(props: Props) {
        super(props);
        this.state = {
            name: '',
            description: ''
        }
    }

    render() {
        const {name, description} = this.state;
        const title = this.props.editId ? 'Edit Category' : 'Create Category';

        return (
            <div>
                <Elevation z={12} className="page-title">{title}</Elevation>
                <Elevation z={10} className="page-content">
                    <TextField
                        id="category_name"
                        value={name}
                        onChange={(event: any) => this.setState({name: event.target.value})}
                        required
                        label="Name"/>
                    <br/>
                    <TextField
                        id="category_description"
                        value={description}
                        required
                        onChange={(event: any) => this.setState({description: event.target.value})}
                        label="Description"/>
                </Elevation>
                <Button className="page-footer" raised onClick={this.onSave}>
                    <ButtonIcon icon="save"/>
                    Save
                </Button>
                <Button className="page-footer" raised onClick={this.props.onBack}>
                    <ButtonIcon icon="arrow_back"/>
                    Back
                </Button>
            </div>
        )

    }

    private onSave = () => {
        const {editId} = this.props;

        let body: any = {};
        let method = 'POST';
        if (editId) {
            body.id = editId;
            method = 'PUT';
        }

        body.name = this.state.name;
        body.description = this.state.description;
        body.parent = this.props.parent;

        fetch(`${BASE_URL}/api/v1/category`,
            {
                method: method,
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(body)
            })
            .then(response => response.json())
            .then(() => this.props.onBack())
    };

}

export default CategoryForm;