import React, {Component} from 'react';
import CategoryList from './CategoryList';
import CategoryForm from './CategoryForm';
import ProductForm from '../product/ProductForm';
import ProductDetails from '../product/ProductDetails';

interface State {
    page: Page,
    editId?: number,
    parent?: number
}

enum Page {
    LIST,
    CATEGORY_EDIT,
    PRODUCT_EDIT,
    PRODUCT_DETAILS
}

class CategoryPage extends Component<any, State> {

    constructor(props: any) {
        super(props);
        this.state = {
            page: Page.LIST
        }
    }

    render() {
        const page = this.state.page;

        let content = (
            <CategoryList
                onShowProduct={this.onShowDetails}
                onCreateProduct={this.onEditProduct}
                onBack={this.onBack}
                onEdit={this.onEditCategory}
            />
        );
        if (page === Page.CATEGORY_EDIT) {
            content = <CategoryForm
                editId={this.state.editId}
                parent={this.state.parent}
                onBack={this.onBack}/>
        }
        if (page === Page.PRODUCT_EDIT) {
            content = <ProductForm parent={this.state.parent!} onBack={this.onBack}/>
        }
        if (page === Page.PRODUCT_DETAILS) {
            content = <ProductDetails productId={this.state.editId!} onBack={this.onBack}/>
        }

        return (
            <div>
                {content}
            </div>
        )
    }

    private onBack = () => {
        this.setState({
            page: Page.LIST,
            parent: undefined,
            editId: undefined
        });
        this.forceUpdate();
    };

    private onEditCategory = (id?: number, parent?: number) => {
        this.setState({
            page: Page.CATEGORY_EDIT,
            parent,
            editId: id
        });
    };

    private onEditProduct = (parent: number, id?: number) => {
        this.setState({
            page: Page.PRODUCT_EDIT,
            parent,
            editId: id
        });
    };

    private onShowDetails = (id: number) => {
        this.setState({
            page: Page.PRODUCT_DETAILS,
            editId: id
        });
    };

}

export default CategoryPage;