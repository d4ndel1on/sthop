export interface Page<T> {
    content: T[]
}

export interface Category {

    id?: number
    name: string
    description: string
    parent: number

}

export interface Product {

    id?: number
    name: string
    description: string
    parent: number
    price: number
    available: boolean

}