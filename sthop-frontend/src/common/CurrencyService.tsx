export function priceToEuro(price: number, currency: string): Promise<number> {
    return fetch(`https://sthop.s0na.de/api/v1/fixer`)
        .then(response => response.json())
        .then((response) => {
            const rate: number = response.rates[currency];
            return Math.round((price / rate) * 100) / 100;
        });
}